<?php

if (!defined('ABSPATH')) exit;
function enqueue_custom_stylesheets()
{
	
	wp_enqueue_style(
        'qds_custom',
        get_stylesheet_directory_uri() . '/css/qds_custom.css',
        array(),
        time()
    );
	
}
function enqueue_admin_stylesheets()
{
    wp_register_style(
        'qds_custom_admin_css',
        get_stylesheet_directory_uri() . '/css/qds_custom_admin.css',
        array(),
        time()
    );
    wp_enqueue_style('qds_custom_admin_css');
}

add_action('wp_enqueue_scripts', 'enqueue_custom_stylesheets',12);
//add_action('admin_enqueue_scripts', 'enqueue_admin_stylesheets', 12);