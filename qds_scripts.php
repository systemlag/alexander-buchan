<?php

if (!defined('ABSPATH')) exit;
function dequeue_parent_scripts_enqueue_modified()
{
    
	wp_enqueue_script(
        'custom_scripts',
        get_stylesheet_directory_uri() . '/js/qds_custom.js',
        array('jquery'),
        time(),
        true
    );
    wp_enqueue_script(
        'custom_scripts',
        get_stylesheet_directory_uri() . '/js/jquery.matchHeight-min.js',
        array('jquery'),
        time(),
        true
    );
}

add_action('wp_enqueue_scripts', 'dequeue_parent_scripts_enqueue_modified');
